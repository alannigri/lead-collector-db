package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {


    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setNome("Alan");
        lead.setEmailPessoal("alannigri@hotmail.com");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        produto = new Produto();
        produto.setNomeProduto("Café");
        produto.setDescricao("Cafezinho de toda manhã");
        produto.setPreco(9.99);
        produto.setCodigoProduto(1);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class)))
                .then(leadObjeto -> {
                    lead.setId(1);
                    lead.setData(LocalDate.now());
                    return lead;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                //saber o status - se for exception -    .isBadRequest()
                .andExpect(MockMvcResultMatchers.status().isCreated())
                //bater o conteudo ID
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                //bater a data
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataDeCadastro", CoreMatchers.equalTo(LocalDate.now().toString())));
    }

    @Test
    public void testarExibirTodos() throws Exception {
        Lead lead1 = new Lead();
        Iterable<Lead> leads = Arrays.asList(lead, lead1);

        Mockito.when(leadService.buscarTodos()).thenReturn(leads);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                //saber o status - se for exception -    .isBadRequest()
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));

        //bater o conteudo ID
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
        //bater a data
//                .andExpect(MockMvcResultMatchers.jsonPath("$.dataDeCadastro", CoreMatchers.equalTo(LocalDate.now().toString())));

    }

    @Test
    public void testarExibirTodosPorTipoLead() throws Exception {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadService.buscarTodosPorTipoLead(Mockito.any(TipoLeadEnum.class))).thenReturn(leads);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads?tipoDeLead")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorID() throws Exception {
        lead.setId(1);
        Mockito.when(leadService.buscarPorID(Mockito.anyInt())).thenReturn(lead);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIDInvalido() throws Exception {
        lead.setId(1);
        Mockito.when(leadService.buscarPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }


}