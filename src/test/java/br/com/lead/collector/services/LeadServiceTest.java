package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp() {
        lead = new Lead();
        lead.setNome("Alan");
        lead.setEmailPessoal("alannigri@hotmail.com");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setId(1);
        produto = new Produto();
        produto.setNomeProduto("Café");
        produto.setDescricao("Cafezinho de toda manhã");
        produto.setPreco(9.99);
        produto.setCodigoProduto(1);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarPorTodosOsLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);

    }

    @Test
    public void testarSalvarLead() {
        Mockito.when(produtoService.buscarTodosOsCodigos(Mockito.anyList())).thenReturn(produtos);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Thais");
        leadTeste.setEmailPessoal("thaisnigri@hotmail.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setProdutos(produtos);

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getDataDeCadastro());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));
    }

    @Test
    public void testarBuscarTodosPorTipoDeLead() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAllByTipoLead(Mockito.any(Enum.class))).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodosPorTipoLead(lead.getTipoLead());

        Assertions.assertEquals(leads, leadIterable);

    }

    @Test
    public void testarAtualizarLead() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
//        Mockito.when(leadService.buscarPorID(Mockito.anyInt())).thenReturn(lead);
//        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);

        lead.setNome("Thais");
        lead.setEmailPessoal("thaisnigri@hotmail.com");
        lead.setTipoLead(TipoLeadEnum.FRIO);
        lead.setProdutos(produtos);

        Lead leadTeste;
        leadTeste = leadService.atualizarLead(1, lead);

        Assertions.assertEquals(lead, leadTeste);
    }

    @Test
    public void testarAtualizarLeadQueNaoExiste() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.atualizarLead(2, lead);
        });
    }

    @Test
    public void testarDeletarLead(){
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        leadService.deletarLead(Mockito.anyInt());
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }
}
