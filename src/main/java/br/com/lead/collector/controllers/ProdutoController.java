package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto criarProduto(@RequestBody @Valid Produto produto) {
        Produto produto1 = produtoService.criarProduto(produto);
        return produto1;
    }

    @GetMapping
    public Iterable<Produto> consultarProdutos(@RequestParam(name = "nomeProduto", required = false) String nomeProduto,
                                               @RequestParam(name = "preco", required = false) Double preco) {
        if (nomeProduto != null) {
            if (preco != null) {
                Iterable<Produto>  produto = produtoService.consultaProdutoPorNomeAndPreco(nomeProduto, preco);
                return produto;
            } else {
                Iterable<Produto> produto = produtoService.consultarProdutoPorNome(nomeProduto);
                return produto;
            }
        }
        Iterable<Produto> produto = produtoService.buscarTodosProdutos();
        return produto;
    }

    @GetMapping("/{codigoProduto}")
    public Produto consultarProdutoPorID(@PathVariable int codigoProduto) {
        Produto produto = produtoService.buscarPorCodigoProduto(codigoProduto);
        return produto;
    }

    @PutMapping("/{codigoProduto}")
    public Produto alterarProduto(@PathVariable int codigoProduto, @RequestBody Produto produto) {
        try {
            Produto produto1 = produtoService.atualizarProduto(codigoProduto, produto);
            return produto1;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{codigoProduto}")
    public String deletarProduto(@PathVariable int codigoProduto) {
        try {
            produtoService.deletarProduto(codigoProduto);
            return "Deletado!";
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}