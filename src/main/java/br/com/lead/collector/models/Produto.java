package br.com.lead.collector.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "produto")
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigoProduto;

    //    @Column(name = "nome do Produto")
    @NotNull(message = "Informar o nome")
    @NotBlank(message = "Deve ser preenchido um nome para o produto")
    private String nomeProduto;

    @NotNull(message = "Informar a descrição")
    @Min(value = 5)
    @NotBlank(message = "Deve ser escrita uma descrição do produto")
    private String Descricao;

 //   @Min(message = "Deve ser preenchido um valor")
    private double preco;

    public Produto() {
    }

    public int getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(int codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
