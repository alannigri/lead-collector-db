package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoService produtoService;


    public Lead salvarLead(Lead lead){
        List<Integer> codigoProdutos = new ArrayList<>();
        LocalDate data = LocalDate.now();
        for (Produto produto : lead.getProdutos()){
            int id = produto.getCodigoProduto();
            codigoProdutos.add(id);
        }
        List<Produto> produtos = produtoService.buscarTodosOsCodigos(codigoProdutos);
        lead.setProdutos(produtos);
        lead.setData(data);
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodos(){
        return leadRepository.findAll();
    }

    public Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum leadEnum){
        return leadRepository.findAllByTipoLead(leadEnum);
    }

    public Lead buscarPorID(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if(optionalLead.isPresent()){
            return optionalLead.get();
        }
        throw new RuntimeException("Lead não encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
//
        if (leadRepository.existsById(id)){
            lead.setId(id);
            Lead leadObjeto = leadRepository.save(lead);

            return leadObjeto;
        }
        throw new RuntimeException("O lead não foi encontrado");

//        buscarPorID(id); //verifica se lead existe no DB - se nao achar, a exception é estourada em cima
//        lead.setId(id); // seta o ID
//        Lead leadObjeto = salvarLead(lead); //salva Lead
//        return leadObjeto;
    }

    public void deletarLead(int id) {
        if (leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não encontrado");
        }
    }
}
