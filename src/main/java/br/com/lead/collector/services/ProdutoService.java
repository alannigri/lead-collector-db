package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto criarProduto(Produto produto) {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosProdutos() {
        Iterable<Produto> produto = produtoRepository.findAll();
        return produto;
    }

    public Produto buscarPorCodigoProduto(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        throw new RuntimeException("Produto não encontrado");
    }

    public Iterable<Produto> consultarProdutoPorNome(String nome) {
        Iterable<Produto> produto = produtoRepository.findAllByNomeProduto(nome);
        return produto;
    }

    public Iterable<Produto> consultaProdutoPorNomeAndPreco(String nome, double preco) {
        Iterable<Produto> produto = produtoRepository.findByNomeProdutoAndPrecoLessThan(nome, preco);
        return produto;
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setCodigoProduto(id);
            Produto produto1 = salvarProduto(produto);
            return produto1;
        } else {
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public Produto salvarProduto(Produto produto) {
        produtoRepository.save(produto);
        return produto;
    }

    public boolean deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
            return true;
        } else {
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public List<Produto> buscarTodosOsCodigos(List<Integer> id) {
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List) produto;
    }

}
